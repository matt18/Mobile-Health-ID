//
//  Doctor.h
//  mobilehealthid
//
//  Created by Matt Sodomsky on 2014-11-16.
//  Copyright (c) 2014 Matt Sodomsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Doctor : NSObject

@property NSString *name;
@property NSString *phoneNumber;
@property NSString *institution;
@end
